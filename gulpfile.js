'use strict';

const gulp = require('gulp');
const nunjucks = require('gulp-nunjucks');
 
gulp.task('html', () =>
    gulp.src('src/**.html')
        .pipe(nunjucks.compile())
        .pipe(gulp.dest('build'))
);

/* gulp.task('css', function(){
    return gulp.src('client/templates/*.less')
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(gulp.dest('build/css'))
});
 */  
/* gulp.task('js', function(){
    return gulp.src('client/javascript/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/js'))
}); */

gulp.task('build', [ 'html' ]);